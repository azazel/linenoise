Source: linenoise
Section: libs
Priority: optional
Maintainer: Jeremy Sowden <azazel@debian.org>
Homepage: https://github.com/antirez/linenoise
Vcs-Browser: https://salsa.debian.org/azazel/linenoise
Vcs-Git: https://salsa.debian.org/azazel/linenoise.git
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0

Package: liblinenoise0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Minimal replacement for readline (shared library)
 A minimal, zero-config, BSD licensed, readline replacement used in Redis,
 MongoDB, and Android.
 .
   * Single and multi line editing mode with the usual key bindings implemented.
   * History handling.
   * Completion.
   * Hints (suggestions at the right of the prompt as you type).
   * About 1,100 lines of BSD license source code.
   * Only uses a subset of VT100 escapes (ANSI.SYS compatible).
 .
 This package contains the shared library.

Package: liblinenoise-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, liblinenoise0 (= ${binary:Version})
Description: Minimal replacement for readline (development files)
 A minimal, zero-config, BSD licensed, readline replacement used in Redis,
 MongoDB, and Android.
 .
   * Single and multi line editing mode with the usual key bindings implemented.
   * History handling.
   * Completion.
   * Hints (suggestions at the right of the prompt as you type).
   * About 1,100 lines of BSD license source code.
   * Only uses a subset of VT100 escapes (ANSI.SYS compatible).
 .
 This package contains the header file and example code.
